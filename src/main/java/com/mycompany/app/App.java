package com.mycompany.app;

import javax.servlet.http.Cookie;

/**
 * Hello world!
 */
public class App {

    private final String message = "Hello World!";

    public App() {
    }

    public static void main(String[] args) {
        Cookie c = new Cookie("someData","someMoredata");
        c.setSecure(false);
        System.out.println(new App().getMessage());
    }

    private final String getMessage() {
        return message;
    }

}
